package org.techforumist.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.techforumist.jwt.domain.Auction;

public interface AuctionRepository extends JpaRepository<Auction, Long> {
}
