package org.techforumist.jwt.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.techforumist.jwt.domain.Auction;
import org.techforumist.jwt.repository.AuctionRepository;

@RestController
public class AuctionCreateRestController {

	@Autowired
	private AuctionRepository auctionRepository;

	@RequestMapping(value = "/auctionCreate", method = RequestMethod.POST)
	public ResponseEntity<Auction> createUser(@RequestBody Auction auction) {
		return new ResponseEntity<Auction>(auctionRepository.save(auction), HttpStatus.OK);
	}
}
