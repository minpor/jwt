angular.module('JWTDemoApp').controller('AuctionCreateController', function($http, $scope, AuthService) {
    $scope.submit = function() {

        var j = {"userId": ""+ AuthService.user.id +""};
        const auction = Object.assign(j, $scope.auction);

        $http.post('auctionCreate', auction).success(function(res) {
            $scope.auction = null;
            $scope.auctionCreate.$setPristine();
            $scope.message = "Аукцион создан!";
        }).error(function(error) {
            $scope.message = error.message;
        });
    }
});

